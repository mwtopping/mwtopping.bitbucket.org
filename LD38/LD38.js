
//globals
var background_color = 20;
var paused = true;
var gameover = false;
var accel = 0.01;

function Bullet(x_, y_, angle_, type_) {
  this.x = x_;
  this.y = y_;
  this.angle = angle_;
  this.speed = 10;
  this.type = type_;

  this.update = function() {
    if (this.type == 'e') {
      this.x -= 0.5*this.speed*sin(this.angle);
      this.y += 0.5*this.speed*cos(this.angle);
    } else {
      this.x -= this.speed*sin(this.angle);
      this.y += this.speed*cos(this.angle);
    }

  }

  this.draw = function() {
    if (this.type == 'f') {
      fill(150, 150, 220);
    } else if (this.type == 'e') {
      fill(150, 220, 150);
    } else {
      fill(220);
    }
    noStroke();
    ellipse(this.x, this.y, 10, 10);
  }

  this.collide = function() {
    if (this.x < 0 || this.x > width) {
      return true;
    }
    if (this.y < 0 || this.y > height) {
      return true;
    }
    if (this.type == 'e') {
      var dx = player.x - this.x;
      var dy = player.y - this.y;
      var dist = sqrt(pow(dx, 2)+pow(dy, 2));
      if (dist < 10) {
        player.crash();
      }
    }
  }
}


// create the enemy class
function Enemy(x_, y_) {
  this.x = x_;
  this.y = y_;
  this.r = 10;
  this.maxspeed = .6+random(.4);

  this.draw = function() {
    fill(20, 200, 20);
    noStroke();
    ellipse(this.x, this.y, this.r);
  }

  this.update = function() {
    // move toward the player at some speed
    var dx = player.x - this.x;
    var dy = player.y - this.y;
    var dist = sqrt(pow(dx, 2)+pow(dy, 2));
    var speed = map(dist, 0, 200, .2*this.maxspeed, this.maxspeed);
    this.x += speed*dx/dist;
    this.y += speed*dy/dist;

    this.r = 10+5*abs(sin(frameCount/30));

    var angle = atan2(dy, dx)-PI/2.;

    if (frameCount % 100 == 0) {
      player.bullets.push(new Bullet(this.x, this.y, angle, 'e'));
    }

    // check if it has crashed with the player
    if (dist < 10) {
      player.crash();
    }
  }

}


// create the player class
function Player() {
  this.x = width/2.;
  this.y = height/2.;
  this.vx = 0.;
  this.vy = 0.;
  this.theta = 0;
  this.world_angle = 0;
  this.width = 20;
  this.height = 30;
  this.speed = 4;
  this.shoot_timer = 0;
  this.shooting = false;
  this.bullets = [];
  this.shoot_speed = 4;

  this.draw = function() {
    fill(100);
    // save the translation state
    //push();
    // move/rotate the player
    push();
    translate(this.x, this.y);
    rotate(this.theta);
    // draw the player
    strokeWeight(3);
    stroke(20, 20, 125);
    triangle(-this.width/2., -this.height/2., 0, this.height/2., this.width/2, -this.height/2.);
    strokeWeight(1);
    stroke(30,180, 250);
    triangle(-this.width/2., -this.height/2., 0, this.height/2., this.width/2, -this.height/2.);
    pop();
    // return to the state
    //pop();
    for (var ii=0; ii<this.bullets.length; ii++) {
      this.bullets[ii].update();
      this.bullets[ii].draw();
    }

  }

  this.update = function() {
    // set the angle to the mouses position
    var angle = atan2((mouseY-this.y), (mouseX-this.x));
    this.theta = angle-PI/2.;
    this.x += this.vx;
    this.y += this.vy;

    // shoot bullets
    if (this.shooting == true) {
      this.shoot_timer += 1;
      if (this.shoot_timer%this.shoot_speed == 0) {
        this.shoot();
      }
    }

    // check bullets
    for (var ii=this.bullets.length-1; ii>0; ii--) {
      if (this.bullets[ii].collide() == true) {
        this.bullets.splice(ii, 1);
      }
    }


    this.detect_wedge();
    var world_rad = this.world_detect();

    // calculate the distance from the world to the player
    var dist = sqrt(pow(this.x-world.x,2) + pow(this.y-world.y,2));

    // check if the player has crashed
    if (dist > world_rad) {
      this.crash();
    }
  }


  this.detect_wedge = function() {
    var angle = atan2((world.x-this.x), (world.y-this.y))+PI;
    this.world_angle = angle;
    return angle;
  }


  this.world_detect = function() {
    var wedge_min_ind = floor(world.nsegments*this.world_angle/(2*PI))%world.nsegments

    // find the radius at this angle
    var wedge_pos = world.nsegments*this.world_angle/(2*PI) - floor(world.nsegments*this.world_angle/(2*PI))

    // uhhhhhh
    //var current_radius = min(world.vertices[wedge_min_ind].mag(), world.vertices[(wedge_min_ind+1)%world.nsegments].mag())+(wedge_pos*abs(world.vertices[wedge_min_ind].mag()- world.vertices[(wedge_min_ind+1)%world.nsegments].mag()))
    var current_radius =world.vertices[wedge_min_ind].mag()+(wedge_pos*(-world.vertices[wedge_min_ind].mag()+ world.vertices[(wedge_min_ind+1)%world.nsegments].mag()))

    return current_radius;
  }

  this.vel_reset = function() {
    this.vx = 0;
    this.vy = 0;
  }


  this.crash = function() {
    this.x = width/2.;
    this.y = height/2.;
    noLoop();
    fill(127);
    textSize(64);
    textAlign(CENTER);
    stroke(200);
    text("Game Over", width/2, height/2);
    textSize(48);
    text(`Score = ${floor(score/1e6)}`, width/2, height/2+60)
    textSize(32);
    text("Press 'space' to restart", width/2, height/2+100);
    world = new World();
    score = 0;
    gameover = true;

  }

  this.shoot = function() {
    this.bullets.push(new Bullet(this.x, this.y, this.theta, 'f'));
    if (buttons[0].mute == true) {
    } else {
      shoot_sound.play(0, 1, .1, 0);
    }
  }

}




function World() {
  this.r = 350;
  this.nsegments = 16;
  this.x = width/2;
  this.y = height/2;
  this.vertices = [];
  this.vert_hit = [];
  this.speeds = [];
  this.vert_r = 50;

  // create an initial segment of lines that form a circle
  //  and push them onto the vertices array
  for (var ii=0; ii<this.nsegments; ii++) {
    var angle = (ii/this.nsegments)*2*PI;
    var temp_r = this.r+random(-50,50);
    var temp_speed = random(.15, .23);
    var x = temp_r*sin(angle);
    var y = temp_r*cos(angle);
    this.vertices.push(createVector(x,y));
    this.speeds.push(temp_speed);
    this.vert_hit.push(false);
  }



  this.draw = function() {
    fill(10);
    beginShape();
    for (var ii=0; ii<this.nsegments; ii++) {
      vertex(this.x+this.vertices[ii].x, this.y+this.vertices[ii].y);
    }
    endShape(CLOSE);

    // draw the wedge that the player is in here
    var p_angle = (player.detect_wedge());
    var wedge = floor(world.nsegments*p_angle/(2*PI))%this.nsegments;

    // diagnostic triangle fill
    // fill(30);
    // noStroke();
    // triangle(this.x, this.y, this.x+this.vertices[wedge].x, this.y+this.vertices[wedge].y, this.x+this.vertices[(wedge+1)%this.nsegments].x, this.y+this.vertices[(wedge+1)%this.nsegments].y)




    for (var ii=0; ii<this.nsegments; ii++) {
      var shimmer = random(200);
      fill(155);
      stroke(80, 50+shimmer);
      strokeWeight(3);
      line(this.x+this.vertices[ii].x, this.y+this.vertices[ii].y, this.x+this.vertices[(ii+1)%this.nsegments].x, this.y+this.vertices[(ii+1)%this.nsegments].y)
      stroke(180, 180, 255, 50+shimmer);
      strokeWeight(1);
      line(this.x+this.vertices[ii].x, this.y+this.vertices[ii].y, this.x+this.vertices[(ii+1)%this.nsegments].x, this.y+this.vertices[(ii+1)%this.nsegments].y)

    }

    for (var ii=0; ii<this.nsegments; ii++) {
      // draw a circle at each wedge
      fill(127, 20, 20, 127);
      strokeWeight(1);
      stroke(255, 80, 80);
      if (this.vert_hit[ii] == true) {
        fill(200, 30, 30);
        this.vert_hit[ii] = false;
      }
      ellipse(this.x+this.vertices[ii].x, this.y+this.vertices[ii].y, this.vert_r, this.vert_r);
    }


  }


  this.update = function() {
    // update the radii of each point
    for (var ii=0; ii<this.nsegments; ii++) {
      this.vertices[ii].setMag(this.vertices[ii].mag()-this.speeds[ii]);
      // check if any bullets it the corners
      for (var jj=player.bullets.length-1; jj>0; jj--) {
        var dist = sqrt(pow(player.bullets[jj].x-(this.x+this.vertices[ii].x), 2)+pow(player.bullets[jj].y-(this.y+this.vertices[ii].y), 2));
        if (dist < (this.vert_r/2.+10)) {
          player.bullets.splice(jj, 1);
          this.vertices[ii].setMag(this.vertices[ii].mag()+50);
          this.vert_hit[ii] = true;
        }
      }
    }
    //this.update_center();
  }


  this.get_volume = function() {
    var temp_v = 0;
    for (var ii=0; ii<this.nsegments; ii++) {
      temp_v += this.vertices[ii].mag();
    }
    return pow(temp_v/this.nsegments, 2);
  }

  this.update_center = function() {
    var temp_cx = 0;
    var temp_cy = 0;
    for (var ii=0; ii<this.nsegments; ii++) {
      temp_cx += this.vertices[ii].x;
      temp_cy += this.vertices[ii].y;
    }
    temp_cx /= this.nsegments;
    temp_cy /= this.nsegments;

    this.x = width/2+temp_cx;
    this.y = height/2+temp_cy;
  }


  this.speed_up = function() {
    for (var ii=0; ii<this.speeds.length; ii++) {
      this.speeds[ii] += accel;
    }
  }
}


function Button(x_, y_, w_, h_) {
  this.x = x_;
  this.y = y_;
  this.w = w_;
  this.h = h_;

  this.draw = function() {
    fill(255);

    rect(this.x, this.y, this.w, this.h);
  }

  this.clicked = function(mx, my) {
    if (mx > this.x && mx < this.x+this.w) {
      if (my > this.y && my < this.y + this.h) {
        return true;
      }
    }
  }

  this.action = function() {
    return;
  }
}


function Volume(x_, y_, w_, h_) {
  Button.call(this, x_, y_, w_, h_);

  this.mute = false;
  this.action = function() {
    if (this.mute == true) {
      background_song.setVolume(.1);
      shoot_sound.setVolume(.1);
      this.mute = false;
    } else if (this.mute == false) {
      background_song.setVolume(0);
      shoot_sound.setVolume(0);

      this.mute = true;
    }
  }

  this.draw = function() {
    noStroke();
    fill(50);
    rect(this.x, this.y, this.w, this.h);
    // draw the square
    fill(127);
    rect(this.x+this.w*1./8, this.y+this.h*5/16, this.w*5/8, this.w*3/8)
    triangle(this.x+this.w/2, this.y+this.h/2, this.x+this.w*7./8., this.y+this.h*7./8., this.x+this.w*7./8., this.y+this.h*1./8.);
    if (this.mute == true) {
      stroke(170);
      strokeWeight(2);
      line(this.x+5, this.y+5, this.x+this.w-5, this.y+this.h-5);
      line(this.x+5, this.y+this.h-5, this.x+this.w-5, this.y+5);
    }
  }
}



var player;
var world;
var background_song;
var shoot_sound;
var buttons = [];
var score = 0;
var enemies = [];
var stars = [];
var star_colors = [];
var nstars = 400;
var w = 800;
var h = 800;

function preload() {
  // load the background music
  background_song = loadSound('LD38.wav');
  shoot_sound = loadSound('shoot.wav');
}

function setup() {

  var colors = ['b', 'r'];
  // set the positions of all the stars
  for (var is = 0; is<nstars; is++) {
    stars.push(createVector(random(w), random(h)));
    star_colors.push(random(colors));
  }

  // create canvas and set background color
  createCanvas(w,h);
  draw_background();
  player = new Player();
  world = new World();

  enemies.push(new Enemy(300, 300));

  background_song.loop();
  background_song.setVolume(.1);

  buttons.push(new Volume(10, 10, 40, 40))
  buttons[0].action();
  if (paused == true) {
    noLoop();
  }

}



// draw the scene
function draw() {
  draw_background();
  fill(220);
  world.draw();
  player.draw();

// draw the enemies
  for (var ie=0; ie<enemies.length; ie++) {
    enemies[ie].draw();
  }

  player.update();
  world.update();
  for (var ie=0; ie<enemies.length; ie++) {
    enemies[ie].update();
  }


  for (var ie=enemies.length; ie>0; ie--) {
    var hit = false;
    // check if an enemy is height
    for (var ib = 0; ib < player.bullets.length; ib++) {
      var dx = enemies[ie-1].x - player.bullets[ib].x;
      var dy = enemies[ie-1].y - player.bullets[ib].y;
      var dist = sqrt(pow(dx, 2)+pow(dy, 2));

      if (dist < 10 && player.bullets[ib].type == 'f') {
        hit = true;
      }
    }

    if (hit == true) {
      enemies.splice(ie-1, 1);
    }

  }

  for (var ii=0; ii<buttons.length; ii++) {
    buttons[ii].draw();
  }

  if (paused == true) {
    textSize(64);
    textAlign(CENTER);
    stroke(200);
    text("Paused", width/2, height/2);
    textSize(48);
    text("Press 'space' to play/pause", width/2, height/2+80);
    textSize(32);
    text("WASD to move", width/2, height/2+130);
    text("Click to shoot", width/2, height/2+170);


  }

  if (frameCount%300 == 0) {
    world.speed_up();
    enemies.push(new Enemy(width/2+random(200), height/2+random(200)));
    console.log("HELLO");

  }

  textSize(32);
  textAlign(CENTER);
  stroke(200);
  text(`Score ${floor(score/1e6)}`, width/2, 40);
  score += world.get_volume();



}


// check for key presses
function keyPressed() {
  if (key == 'W') {
    player.vy += -player.speed;
  }
  if (key == 'A') {
    player.vx += -player.speed;
  }
  if (key == 'S') {
    player.vy += player.speed;
  }
  if (key == 'D') {
    player.vx += player.speed;
  }
  if (key == ' ' ) {
    if (paused == false && gameover == false) {
      noLoop();
      paused = true;
    } else if (paused == true || gameover == true) {
      loop();
      paused = false;
      gameover = false;
    }
  }
}


// when movement keys are released, subtract the speed;
function keyReleased() {
  if (key == 'W') {
    player.vy -= -player.speed;
  }
  if (key == 'A') {
    player.vx -= -player.speed;
  }
  if (key == 'S') {
    player.vy -= player.speed;
  }
  if (key == 'D') {
    player.vx -= player.speed;
  }
}


function draw_background() {
  background(background_color);
  strokeWeight(2);
  for (var ii=0; ii<nstars; ii++) {
    var luminosity = random(150);
    if (star_colors[ii] == 'b') {
      stroke(50+luminosity, 50+luminosity, 105+luminosity);
    } else if (star_colors[ii] = 'r') {
      stroke(80+luminosity, 20+luminosity, 20+luminosity);
    } else {
      stroke(50+luminosity, 50+luminosity, 50+luminosity);
    }


    point(stars[ii].x, stars[ii].y);
  }
}

// shoot bullets
function mousePressed() {
  for (var ii=0; ii<buttons.length; ii++) {
    if (buttons[ii].clicked(mouseX, mouseY) == true) {
      buttons[ii].action();
    } else {
      player.shooting = true;
    }
  }
}

function mouseReleased() {
  player.shooting = false;
  player.shoot_timer = 0;
}

function reset() {
  player = new Player();
  world = new World();
}
